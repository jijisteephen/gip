<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>GPRISM - Home</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Cappuccino Coffee Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link href="//fonts.googleapis.com/css?family=Arizonia&amp;subset=latin-ext" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Timmana&amp;subset=telugu" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Bellefair&amp;subset=hebrew,latin-ext" rel="stylesheet">
	<!-- //Web-Fonts -->

</head>

<body>
	<!-- header -->
	<div class="header">
		<!-- navigation -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<!-- logo -->
			<h1>
				<a class="navbar-brand" href="index.php">
					<img src="img/logonew.png">
				</a>
			</h1>
			<div class="" id="navbarSupportedContent" >
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php" style="font-size: 34px;color: #fff;margin-left:-15%;margin-bottom: 7%">GPRISM -
							<!-- <span class="sr-only">(current)</span> -->
						 Karakulam Panchayat
							<!-- <span class="sr-only">(current)</span> -->
						</a>
					</li>
					
				</ul>
			</div>
			<!-- //logo -->
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
			    aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home
							<!-- <span class="sr-only">(current)</span> -->
						</a>
					</li>
					<li class="nav-item mx-lg-4">
						<a class="nav-link" href="agriculture.php">Agriculture</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link" href="gallery.php">Education</a>
					</li>
					<li class="nav-item mx-lg-4">
						<a class="nav-link" href="menu.php">Health</a>
					</li>
					<li class="nav-item mx-lg-4">
						<a class="nav-link" href="menu.php">Members</a>
					</li>
					<li class="nav-item dropdown mr-lg-4">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
						    aria-expanded="false">
							Others
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="menu.php">Menu</a>
							<a class="dropdown-item scroll" href="#services">Services</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="about.php">Contact Us</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.php">UserLogin</a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- //navigation -->
	</div>
	<!-- //header -->
	<!-- inner banner -->
	<!-- <div class="banner-2">

	</div> -->
	<!-- breadcrumb -->
	<!-- <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="index.php">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">About Us</li>
		</ol>
	</nav> -->
	<!-- breadcrumb -->
	<!-- //inner banner -->

	<div class="about-page-w3ls py-5">
		<div class="container py-xl-5 py-lg-3">
			<h5 class="tittle-w3 text-dark text-center mb-5" style="font-size: 50px">About Karakulam Panchayat</h5>
			<div class="row">
				<div class="col-lg-6 about-w3layouts">
					<img src="img/kara.PNG" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 about-w3layouts-right mt-lg-0 mt-5">
					<h3 class="subheading-wthree mb-2">A Few Words About Karakulam Panchayat</h3>
					<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
						qui ratione voluptatem sequi nesciunt.</p>
					<p class="my-2 pb-4 border-bottom">Aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
					<p>sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
				</div>
			</div>
			<!-- services -->
	<div class="servies py-5" id="services" style="background-color: #e6fde8; padding: 0">
		<div class="container py-xl-5 py-lg-3" style="padding-bottom: 0rem !important">
			<!-- <h5 class="tittle-w3 text-dark text-center mb-sm-5 mb-4">Services</h5> -->
			<div class="d-flex skill_info_wthree_agile">
				<div class="col-md-4 banner_bottom_left">
					<div class="row">
						<div class="col-3 banner_bottom_grid_left text-center mb-3">
							<img src="img/health.png">
						</div>
						<div class="col-9 banner_bottom_grid_right mt-1 mb-4">
							
						</div>
						<h3>84351</h3>
						<!-- <p>Lorem ipsum dolor.</p> -->
					</div>
				</div>
				<!-- <div class="col-md-4 banner_bottom_left text-center mt-md-0 mt-3 mb-md-0 mb-5">
					<img src="images/service.png" alt="" class="img-fluid">
					<div class="banner_bottom_grid_right mt-5 mb-2">
						<h3>Services 2</h3>
					</div>
					<p>Lorem ipsum dolor, consectetur adipiscing elit,morbi viverra lacus commodo felis semper.</p>
				</div> -->
				<div class="col-md-4 banner_bottom_left">
					<div class="row">
						<div class="col-3 banner_bottom_grid_left text-center mb-3">
							<img src="img/jobs.png">
						</div>
						<div class="col-9 banner_bottom_grid_right mt-1 mb-4">
							
						</div>
						<h3 class="stat-timer">100</h3>
						<!-- <p>Lorem ipsum dolor</p> -->
					</div>
				</div>
				<div class="col-md-4 banner_bottom_left">
					<div class="row">
						<div class="col-3 banner_bottom_grid_left text-center mb-3">
							<img src="img/education.png">
						</div>
						<div class="col-9 banner_bottom_grid_right mt-1 mb-4">
							
						</div>
						<h3 class="stat-timer">100</h3>
						<!-- <p>Lorem ipsum dolor.</p> -->
					</div>
				</div>
				<div class="col-md-4 banner_bottom_left">
					<div class="row">
						<div class="col-3 banner_bottom_grid_left text-center mb-3">
							<img src="img/house.png">
						</div>
						<div class="col-9 banner_bottom_grid_right mt-1 mb-4">
							
						</div>
						<h3 class="stat-timer">25</h3>
						<!-- <p>Lorem ipsum dolor, </p> -->
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- //services -->

			<div class="row" style="margin-top: 3%">
				<div class="col-lg-6 about-w3layouts-right mb-lg-0 mb-5">
					<!-- chart 1 -->
					<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 300px;
}
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = [
  {
    country: "USA",
    visits: 23725
  },
  {
    country: "China",
    visits: 1882
  },
  {
    country: "Japan",
    visits: 1809
  },
  {
    country: "Germany",
    visits: 1322
  },
  {
    country: "UK",
    visits: 1122
  },
  {
    country: "France",
    visits: 1114
  },
  {
    country: "India",
    visits: 984
  },
  {
    country: "Spain",
    visits: 711
  },
  {
    country: "Netherlands",
    visits: 665
  },
  {
    country: "Russia",
    visits: 580
  },
  {
    country: "South Korea",
    visits: 443
  },
  {
    country: "Canada",
    visits: 441
  }
];

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "country";
categoryAxis.renderer.minGridDistance = 40;
categoryAxis.fontSize = 11;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 24000;
valueAxis.strictMinMax = true;
valueAxis.renderer.minGridDistance = 30;
// axis break
var axisBreak = valueAxis.axisBreaks.create();
axisBreak.startValue = 2100;
axisBreak.endValue = 22900;
//axisBreak.breakSize = 0.005;

// fixed axis break
var d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
axisBreak.breakSize = 0.05 * (1 - d) / d; // 0.05 means that the break will take 5% of the total value axis height

// make break expand on hover
var hoverState = axisBreak.states.create("hover");
hoverState.properties.breakSize = 1;
hoverState.properties.opacity = 0.1;
hoverState.transitionDuration = 1500;

axisBreak.defaultState.transitionDuration = 1000;
/*
// this is exactly the same, but with events
axisBreak.events.on("over", function() {
  axisBreak.animate(
    [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
    1500,
    am4core.ease.sinOut
  );
});
axisBreak.events.on("out", function() {
  axisBreak.animate(
    [{ property: "breakSize", to: 0.005 }, { property: "opacity", to: 1 }],
    1000,
    am4core.ease.quadOut
  );
});*/

var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.categoryX = "country";
series.dataFields.valueY = "visits";
series.columns.template.tooltipText = "{valueY.value}";
series.columns.template.tooltipY = 0;
series.columns.template.strokeOpacity = 0;

// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
series.columns.template.adapter.add("fill", function(fill, target) {
  return chart.colors.getIndex(target.dataItem.index);
});

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>
amCharts
					<!-- end chart 1 -->
				</div>
				<div class="col-lg-6 about-w3layouts-left mb-lg-0 mb-5">
					<!-- chart 2 -->
					<style>
#chartdiv1 {
  width: 100%;
  height: 300px;
}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv1", am4charts.PieChart);

// Add data
chart.data = [ {
  "country": "Lithuania",
  "litres": 501.9
}, {
  "country": "Czech Republic",
  "litres": 301.9
}, {
  "country": "Ireland",
  "litres": 201.1
}, {
  "country": "Germany",
  "litres": 165.8
}, {
  "country": "Australia",
  "litres": 139.9
}, {
  "country": "Austria",
  "litres": 128.3
}, {
  "country": "UK",
  "litres": 99
}, {
  "country": "Belgium",
  "litres": 60
}, {
  "country": "The Netherlands",
  "litres": 50
} ];

// Set inner radius
chart.innerRadius = am4core.percent(50);

// Add and configure Series
var pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "litres";
pieSeries.dataFields.category = "country";
pieSeries.slices.template.stroke = am4core.color("#fff");
pieSeries.slices.template.strokeWidth = 2;
pieSeries.slices.template.strokeOpacity = 1;

// This creates initial animation
pieSeries.hiddenState.properties.opacity = 1;
pieSeries.hiddenState.properties.endAngle = -90;
pieSeries.hiddenState.properties.startAngle = -90;

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv1"></div>
					<!-- end chart2 -->
				</div>
			</div>


















<div class="row" style="margin-top: 3%">
				<div class="col-lg-6 about-w3layouts-right mb-lg-0 mb-5">
					<!-- chart 3 -->
<style>
#chartdiv3 {
  width: 100%;
  height: 300px;
}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv3", am4charts.PieChart3D);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = [
  {
    country: "Lithuania",
    litres: 501.9
  },
  {
    country: "Czech Republic",
    litres: 301.9
  },
  {
    country: "Ireland",
    litres: 201.1
  },
  {
    country: "Germany",
    litres: 165.8
  },
  {
    country: "Australia",
    litres: 139.9
  },
  {
    country: "Austria",
    litres: 128.3
  }
];

chart.innerRadius = am4core.percent(40);
chart.depth = 120;

chart.legend = new am4charts.Legend();

var series = chart.series.push(new am4charts.PieSeries3D());
series.dataFields.value = "litres";
series.dataFields.depthValue = "litres";
series.dataFields.category = "country";
series.slices.template.cornerRadius = 5;
series.colors.step = 3;

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv3"></div>
					<!-- end chart 3 -->
				</div>
				<div class="col-lg-6 about-w3layouts-left mb-lg-0 mb-5">
					<!-- chart 4 -->
<style>
#chartdiv4 {
  width: 100%;
  height: 300px;
}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv4", am4charts.XYChart3D);

// Add data
chart.data = [{
  "country": "USA",
  "visits": 4025
}, {
  "country": "China",
  "visits": 1882
}, {
  "country": "Japan",
  "visits": 1809
}, {
  "country": "Germany",
  "visits": 1322
}, {
  "country": "UK",
  "visits": 1122
}, {
  "country": "France",
  "visits": 1114
}, {
  "country": "India",
  "visits": 984
}, {
  "country": "Spain",
  "visits": 711
}, {
  "country": "Netherlands",
  "visits": 665
}, {
  "country": "Russia",
  "visits": 580
}, {
  "country": "South Korea",
  "visits": 443
}, {
  "country": "Canada",
  "visits": 441
}, {
  "country": "Brazil",
  "visits": 395
}, {
  "country": "Italy",
  "visits": 386
}, {
  "country": "Australia",
  "visits": 384
}, {
  "country": "Taiwan",
  "visits": 338
}, {
  "country": "Poland",
  "visits": 328
}];

// Create axes
let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "country";
categoryAxis.renderer.labels.template.rotation = 270;
categoryAxis.renderer.labels.template.hideOversized = false;
categoryAxis.renderer.minGridDistance = 20;
categoryAxis.renderer.labels.template.horizontalCenter = "right";
categoryAxis.renderer.labels.template.verticalCenter = "middle";
categoryAxis.tooltip.label.rotation = 270;
categoryAxis.tooltip.label.horizontalCenter = "right";
categoryAxis.tooltip.label.verticalCenter = "middle";

let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Countries";
valueAxis.title.fontWeight = "bold";

// Create series
var series = chart.series.push(new am4charts.ColumnSeries3D());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "country";
series.name = "Visits";
series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;
columnTemplate.stroke = am4core.color("#FFFFFF");

columnTemplate.adapter.add("fill", function(fill, target) {
  return chart.colors.getIndex(target.dataItem.index);
})

columnTemplate.adapter.add("stroke", function(stroke, target) {
  return chart.colors.getIndex(target.dataItem.index);
})

chart.cursor = new am4charts.XYCursor();
chart.cursor.lineX.strokeOpacity = 0;
chart.cursor.lineY.strokeOpacity = 0;

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv4"></div>
					<!-- end chart4 -->
				</div>
			</div>




		</div>
	</div>

	<!-- //team -->
	<!-- video play -->
	<!-- <div class="about-bottom text-center py-5">
		<div class="container py-xl-5 py-lg-3">
			<p class="text-white">Cappuccino Coffee</p>
			<h4 class="text-white mx-auto font-italic my-4">Experience The Amazing & Rich Flavour</h4>
		</div>
	</div> -->
	<!-- //video play -->

	<!-- Team -->
	

	

	<!-- footer -->
	<footer>
		
		<div class="copy-right-grids py-3">
			<p class="footer-gd text-center text-white">© 2020 GPRISM. All Rights Reserved | Design by
				<a href="#" target="_blank">Grameena Patana Kendram</a>
			</p>
		</div>
	</footer>
	<!-- //footer -->


	<!-- Js files -->
	<!-- JavaScript -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->
	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smooth scrolling -->

	<!-- move-top -->
	<script src="js/move-top.js"></script>
	<!-- easing -->
	<script src="js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="js/coffee.js"></script>
	    <script src="js/custom.js"></script>

	<!-- //Js files -->

</body>

</html>