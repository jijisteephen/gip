<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>GPRISM - Index </title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Cappuccino Coffee Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link href="//fonts.googleapis.com/css?family=Arizonia&amp;subset=latin-ext" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Timmana&amp;subset=telugu" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Bellefair&amp;subset=hebrew,latin-ext" rel="stylesheet">
	<!-- //Web-Fonts -->

</head>

<body>
	<!-- header -->
	<div class="header">
		<!-- navigation -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<!-- logo -->
			<h1>
				<a class="navbar-brand" href="index.php" style="">
					<img src="img/logonew.png">
				</a>

			</h1>
			<div class="" id="navbarSupportedContent" >
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php" style="font-size: 34px;color: #fff; margin-bottom: 3%">Geospatial Panchayat Resource Information System (GPRISM)
							<!-- <span class="sr-only">(current)</span> -->
						</a>
					</li>
					<li class="nav-item mx-lg-4 active">
						<a class="nav-link" href="index.php" style="font-size: 12px;color: #fff;margin-left: 20">Unique Visitors : 7875853<br>
              Updated On : 20-02-2020</a>
					</li>
					
				</ul>
			</div>
			<!-- //logo -->
			<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
			    aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button> -->
			
		</nav>
		<!-- //navigation -->
	</div>
	<!-- //header -->
	<!-- inner banner -->
	<!-- <div class="banner-2">

	</div> -->
	<!-- breadcrumb -->
	<!-- <nav aria-label="breadcrumb">
		<ol class="breadcrumb" >
			<li class="breadcrumb-item">
				<a href="index.php">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">About Us</li>
		</ol>
	</nav> -->
	<!-- breadcrumb -->
	<!-- //inner banner -->

	<div class="about-page-w3ls py-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="row">
				<div class="col-lg-6 about-w3layouts">
					<img src="img/kerala.png" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 about-w3layouts-right mt-lg-0 mt-5">
					<nav aria-label="breadcrumb">
		<ol class="breadcrumb" >
			<li class="breadcrumb-item" style="">
				<select class="form-control" style="">
                  <option>District</option>
                </select>


			</li>
			<li class="breadcrumb-item">
				<select class="form-control" style="margin-left: 14%">
                  <option>Type</option>
                </select>
			</li>
			<li class="breadcrumb-item">
				<select class="form-control" style="margin-left: 20%">
                  <option>Panchayat</option>
                </select>
			</li>

			<li class="breadcrumb-item active" aria-current="page">
				<a href="home.php">
					<input type="button" class="form-control" value="GO" style="margin-left: 100%">
				</a>
			</li>
		</ol>
	</nav>
								<h5 class="tittle-w3 text-dark text-center mb-5">About Gprism</h5>

					<!-- <h3 class="subheading-wthree mb-2">A Few Words About Us</h3> -->
					<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
						qui ratione voluptatem sequi nesciunt.</p>
					<p class="my-2 pb-4 border-bottom">Aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
					<p>sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
				</div>
			</div>
		<!-- 	<div class="about-bottom-w3l my-lg-5 my-4 py-5">
				<ul>
					<li>
						<h5>2000+</h5>
						<p>Daily Regular Customers</p>
					</li>
					<li>
						<h5>3600+</h5>
						<p>Excellent Coffee Flavors</p>
					</li>
					<li>
						<h5>1200+</h5>
						<p>Different Cappuccino Coffee</p>
					</li>
				</ul>
			</div> -->
			<div class="row" style="margin-top: 3%;">
				<div class="col-lg-6 about-w3layouts-right mb-lg-0 mb-5">
					<h3 class="subheading-wthree mb-2">A Few Words About Our History</h3>
					<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
						qui ratione voluptatem sequi nesciunt.</p>
					<p class="my-2 pb-4 border-bottom">Aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
					<p>sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
				</div>
				<div class="col-lg-6 about-w3layouts">
					<img src="img/GIPlogin.png" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</div>

	

	<!-- footer -->
	<footer>
		
		<div class="copy-right-grids py-3">
			<p class="footer-gd text-center text-white">© 2020 GPRISM. All Rights Reserved | Design by
				<a href="#" target="_blank">Grameena Patana Kendram</a>
			</p>
		</div>
	</footer>
	<!-- //footer -->


	<!-- Js files -->
	<!-- JavaScript -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->
	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smooth scrolling -->

	<!-- move-top -->
	<script src="js/move-top.js"></script>
	<!-- easing -->
	<script src="js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="js/coffee.js"></script>

	<!-- //Js files -->

</body>

</html>